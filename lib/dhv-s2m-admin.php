<?php

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

class DHV_S2M_Admin
{
	public function __construct()
	{
		add_action('admin_init', array($this, 'admin_init'));
		add_action('admin_menu', array($this, 'admin_menu'));
	}

	public function admin_init()
	{
		global $dhv_s2m_option;
		$conf = get_option($dhv_s2m_option);

		add_settings_section('dhv-s2m-settings-section', 'Adressierung', array($this, 'admin_section'), 'dhv-s2m-settings-page');

		add_settings_field(
		$dhv_s2m_option .'[URLAPI]',
		'API URL',
		array($this, 'create_lineedit'),
		'dhv-s2m-settings-page',
		'dhv-s2m-settings-section',
		array($dhv_s2m_option .'[URLAPI]', $conf['url_api'])
		);

		add_settings_field(
		$dhv_s2m_option .'[URLFEEDS]',
		'Feed URL',
		array($this, 'create_lineedit'),
		'dhv-s2m-settings-page',
		'dhv-s2m-settings-section',
		array($dhv_s2m_option .'[URLFEEDS]', $conf['url_feeds'])
		);

		add_settings_field(
		$dhv_s2m_option .'[URLREPORT]',
		'Report URL',
		array($this, 'create_lineedit'),
		'dhv-s2m-settings-page',
		'dhv-s2m-settings-section',
		array($dhv_s2m_option .'[URLREPORT]', $conf['url_report'])
		);

		register_setting('dhv-s2m-settings-group', $dhv_s2m_option, array($this, 'admin_validate'));
	}

	public function admin_menu()
	{
		add_options_page('DHV S2M Einstellungen', 'DHV S2M', 'manage_options', 'dhv-s2m-settings-page', array($this, 'admin_page'));
	}

	public function admin_section()
	{}

	public function admin_page()
	{
		if (!current_user_can('manage_options'))
			wp_die(__('You do not have sufficient permissions to manage options for this site.'));

		$this->common_header('DHV S2M Einstellungen');
		echo '<form method="post" action="options.php">';
		settings_fields('dhv-s2m-settings-group');
		do_settings_sections('dhv-s2m-settings-page');
		echo
		'
			<p class="submit">
			<input name="Submit" type="submit" class="button-primary" value="Einstellungen speichern" />
			</p>
			</form>
			';
		$this->common_footer();
	}

	public function admin_validate($input)
	{
		return array(
			'url_api' => $input['URLAPI'],
			'url_feeds' => $input['URLFEEDS'],
			'url_report' => $input['URLREPORT']
			);
	}

	public function create_lineedit($arg)
	{
		$name  = $arg[0];
		$value = $arg[1];
		echo '<input style="width:100%" type="text" name="'.$name.'" value="'.$value.'" />';
	}

	public function common_header($title, $subtitle = null)
	{
		if (!isset($subtitle))
			$subtitle = 'For more information about this plugin please visit <a href="http://weed-cube.com">http://weed-cube.com</a>';
		echo
		'
			<div class="wrap">
			<div class="icon32" id="icon-options-general">
			<br />
			</div>
			<h2>'.$title.'</h2>
				'.$subtitle.'
					';

		$advertisement = TRUE;
		if ($advertisement){
			echo
			'
				<h3>Advertisement</h3>
				<script type="text/javascript"><!--
				google_ad_client = "ca-pub-7930794124444982";
				/* WeedCube_DHV_Streckmittelmeldungen */
				google_ad_slot = "8647057292";
				google_ad_width = 728;
				google_ad_height = 90;
				//-->
				</script>
				<script type="text/javascript"
				src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
				</script>
				<hr />
				';
		}
	}

	public function common_footer()
	{
		echo '</div>';
	}
}

new DHV_S2M_Admin();

?>