<?php

/* Notes on...
 *
* GOOGLE MAP
*
*  - https://developers.google.com/maps/documentation/
*  - https://developers.google.com/maps/documentation/imageapis/
*  - https://developers.google.com/maps/documentation/javascript/
*
* TYPES:
*  - roadmap (default)
*  - satellite
*  - terrain
*  - hybrid
*
*  GOOGLE GEOCODING
*  - https://developers.google.com/maps/documentation/geocoding
*/

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

class DHV_S2M
{
	public function __construct()
	{
		add_action('widgets_init', create_function('', 'register_widget("DHV_S2M_Widget");'));
		add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
		add_shortcode('DHVS2M', array('DHV_S2M', 'create_map'));
		add_shortcode('DHVS2MREPORT', array('DHV_S2M', 'create_submit_report'));
	}

	public static function enqueue_scripts()
	{
		wp_enqueue_style('dhv-s2m-style', plugins_url('dhv-s2m/style/dhv-s2m.css'));
	}

	public static function create_s2m_api_query($action, $params = null)
	{
		global $dhv_s2m_option;
		$opts = get_option($dhv_s2m_option);
		$query = $opts['url_api'] . '&dhv-s2m-api[action]=' . urlencode($action);
		if (isset($params)){
			$params['application']  = 'DHV-S2M 1.0.0';
			$params['remote_server']  = $_SERVER['SERVER_NAME'];
			$params['remote_client']  = $_SERVER['REMOTE_ADDR'];
		}else{
			$params = array(
				'application' => 'DHV-S2M 1.0.0',
				'remote_server' => $_SERVER['SERVER_NAME'],
				'remote_client' => $_SERVER['REMOTE_ADDR']
				);
		}
		foreach($params as $key => $value)
			if (is_array($value)) $params[$key] = implode(',', $value);
		$params = http_build_query($params);
		$params = str_replace('&', '|', $params);
		$params = str_replace('=', ':', $params);
		$query = $query . "&dhv-s2m-api[params]=" . $params;
		return $query;
	}

	public static function create_map($atts = null, $content = null)
	{
		extract(shortcode_atts(array(
		'maptype' => 'terrain',
		'size' => '640x640',
		'zoom' => '6',
		'language' => 'de',
		'region' => 'de',
		'center' => 'Deutschland'
			),
			$atts ? $atts : array()));
		/* prepare google maps api call */
		$api ='http://maps.googleapis.com/maps/api/staticmap?';
		$api .= http_build_query(array(
			'sensor' => 'false',
			'size' => $size,
			'zoom' => $zoom,
			'maptype' => $maptype,
			'language' => $language,
			'region' => $region,
			'center' => $center
		));
		$markers = '';
		/* fetch xml document from S3M and transform messages to markers */
		global $dhv_s2m_option;
		$opts = get_option($dhv_s2m_option);
		$url = $opts['url_feeds'] . "&type=3";		
		$xml = file_get_contents($url);	
		$limit = 0;
		$threshold = 1;		
		if (!empty($xml)){				
			$xml = new SimpleXMLElement($xml);	
			$marker_cap = $markers;
			foreach ($xml->MELDUNG as $c){
				if ($c->CUT_COUNT >= $threshold){
					$color = "blue";
					if ($c->CUT_COUNT > 9) $color = "red";
					else if ($c->CUT_COUNT > 5) $color = "yellow";
					$markers .= '&markers=' . urlencode("color:$color|label:$c->CUT_COUNT|size:med|");
					$markers .= urlencode($c->GEO_LAT . ',' . $c->GEO_LNG . '|');
					if (strlen($api . $markers) > 2048){
						$markers = $marker_cap;
						break;
					}else{
						++$limit;
						$marker_cap = $markers;
					}
				}
			}
			/* done; output image linked to google maps */
			$api .= $markers;		
		}	
		
		return
		"
		<div id=\"dhv-s2m-map-credits\">
		<p>Ein Service des <a href=\"http://hanfverband.de\">Deutschen Hanf Verbandes</a></p>
		</div>
		<div id=\"dhv-s2m-map-container\">
		<a target=\"_blank\" href=\"http://hanfverband.de/index.php/themen/streckmittel\"><img width=\"100%\" src=\"$api\"></img></a>
		</div>
		<div id=\"dhv-s2m-map-info\">
		<p>* Maximal $limit Meldungen &amp; Schwellenwert $threshold</p>
		</div>
		";
	}

	public static function create_submit_report($atts = null, $content = null)
	{
		extract(shortcode_atts(array(
		'maptype' => 'terrain',
		),
		$atts ? $atts : array()));
		global $dhv_s2m_option;
		$opts = get_option($dhv_s2m_option);

		if (isset($_POST['dhv-s2m-api'])){
			$params = $_POST['dhv-s2m-api'];
			$query = DHV_S2M::create_s2m_api_query('receive-report', $params);			
			$xml = file_get_contents($query);			
			return $xml;
		}else echo "WTF";

		$side_effects_html = '';
		$side_effects = array(
			'Kopfschmerzen',
			'Halsschmerzen',
			'Bauchschmerzen',
			'&Uuml;belkeit/Erbrechen',
			'Allergien',
			'Atemwegserkrankungen',
			'Angstzust&auml;nde',
			'Kreislaufprobleme',
			'Konzentrationsst&ouml;rungen',
			'Schlafprobleme',
			'Zittern/Tremor',
			'Gleichgewichtsst&ouml;rungen',
			'Vergiftungen',
			'L&auml;hmungen',
			'Andere Effekte'
		);
		for ($i=0; $i<count($side_effects); $i+=2){
			if (count($side_effects) > $i+1){
				$side_effects_html .=
				'
				<tr>
					<td>'.$side_effects[$i].'</td>
					<td><input type="checkbox" name="dhv-s2m-api[sideeffects][]" value="'.$side_effects[$i].'" /></td>
					<td><input type="checkbox" name="dhv-s2m-api[sideeffects][]" value="'.$side_effects[$i+1].'" /></td>
					<td>'.$side_effects[$i+1].'</td>
				</tr>
				';
			}else{
				$side_effects_html .=
				'
				<tr>
					<td>'.$side_effects[$i].'</td>
					<td><input type="checkbox" name="dhv-s2m-api[sideeffects][]" value="'.$side_effects[$i].'" /></td>
					<td></td>
					<td></td>
				</tr>
				';
			}
		}

		return
		'
			<div >
			<form method="post">
			<table class="dhv-s2m-report-cutting-container">
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Art der Verunreinigung
			</td>
			<td class="dhv-s2m-submit-report-option-column">
			<select name="dhv-s2m-api[cutting]">
			<option>unbekanntes Streckmittel</option>
			<option>Brix</option>
			<option>Blei</option>
			<option>Sand</option>
			<option>Schimmel</option>
			<option>Cadmium</option>
			<option>Quecksilber</option>
			<option>sonst. Metalle</option>
			<option>Zucker</option>
			<option>Mehl</option>
			<option>St&auml;rke</option>
			<option>Haarspray</option>
			<option>D&uuml;ngerr&uuml;ckst&auml;nde</option>
			<option>Pestizide</option>
			<option>Fungizide</option>
			<option>sonstiges Streckmittel</option>
			<option>mehrere Streckmittel</option>
			</select>
			</td>
			</tr>
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Stadt oder Region
			</td>
			<td class="dhv-s2m-submit-report-option-column">
			<input type="text" name="dhv-s2m-api[city]"/>
			</td>
			</tr>
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Die ersten 2 Ziffern der Postleitzahl
			</td>
			<td class="dhv-s2m-submit-report-option-column">
			<input type="text" name="dhv-s2m-api[postcode]"/>
			</td>
			</tr>
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Herkunft des Cannabis
			</td>
			<td class="dhv-s2m-submit-report-option-column">
			<select name="dhv-s2m-api[origin]">
			<option>keine Angabe</option>
			<option>Freund, Bekannter - Homegrow&nbsp;</option>
			<option>Privat - Dealer</option>
			<option>Coffeeshop, legale Verkaufstelle</option>
			<option>Laden, illegale Verkaufstelle</option>
			<option>Stra&szlig;enhandel, Park, &&Ouml;uml;ffentlichkeit</option>
			<option>Apotheke, Arzt</option>
			</select>
			</td>
			</tr>
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Beschreibung
			</td>
			<td class="dhv-s2m-submit-report-option-column">
			<input type="text" name="dhv-s2m-api[description]" />
			</td>
			</tr>
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Nebenwirkung
			</td>
			<td class="dhv-s2m-submit-report-option-column">
				<table>
				'
				. $side_effects_html .
				'
				</tr>
				</table>
			</td>
			</tr>
			<tr>
			<td class="dhv-s2m-submit-report-label-column">
			Beschreibung der Nebenwirkungen
			</td>
			<td class="dhv-s2m-submit-report-option-column">
			<input type="text" name="dhv-s2m-api[sideeffects_description]" />
			</td>
			</tr>
			<tr>
			</table>
			<input type="submit" name="dhv-s2m-api[submit-report]" value="Streckmittel Melden" />
			</form>
			</div>
			';
	}
};

class DHV_S2M_Widget extends WP_Widget
{
	public function __construct()
	{
		parent::__construct('dhv_s2m_map_widget', 'DHV Streckmittelkarte',
			array( 'description' => __( 'Die DHV Streckmittelmeldungen visualisiert mit Google Maps', 'text_domain' ), ));
	}

	public function widget($args, $instance)
	{
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		if (!empty( $title ))
			echo $before_title . $title . $after_title;
		global $dhv_s2m;
		echo $dhv_s2m->create_map();
		echo '<div><a href="'.home_url('streckmittelmelder').'">Streckmittel melden</a></div>';
		echo $after_widget;
	}

	public function update($new_instance, $old_instance)
	{
		$instance = array();
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}

	public function form($instance)
	{
		if (isset($instance['title'])){
			$title = $instance['title'];
		}else{
			$title = __('New title', 'text_domain');
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}
};

global $dhv_s2m;
$dhv_s2m = new DHV_S2M();

?>