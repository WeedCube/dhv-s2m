=== DHV-S2M ===
Contributors: weedcube
Donate link: http://weed-cube.com/
Tags: hanfverband, dhv, hanf, cannabis, streckmittel, streckmittelkarte 
Requires at least: 3.0.1
Tested up to: 3.4.2
Stable tag: 3.4.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Streckmittelmeldungen als Karten in Artikeln, Steiten oder als Fenster einbinden

== Description ==

Mit diesem Plugin k�nnen sie die Streckmittelmeldungen des Deutschen Hanf Verbandes 
als Karten in Artikeln, Seiten oder als Fenster einbinden.  

== Installation ==

	1. Laden Sie den Plugin auf den gew�nschten Blog
	2. Aktivieren Sie den Plugin im Administrationsbereich im Men� "Plugins"

== Frequently Asked Questions ==

= Wie f�ge ich ein Fenster hinzu? =

Das Streckmittel Fenster kann im Administrationsbereich unter "Design=>Widgets" hinzugef�gt werden. 

= Wie f�ge ich eine Karte in einen Artikel oder eine Seite ein? =

Zum einf�gen einer Karte in einen Artikel oder eine Seite muss der [DHVS2M] Tag genutzt werden. 

= Wie kann ich das Aussehen einer Karte ver�ndern? =

Zum ver�ndern des Aussehens einer Karten k�nnen dem [DHVS2M] Tag Attribute hinzugef�gt werden.
Derzeit werden die folgenden Attribute unterst�tzt:

	maptype=[terrain|roadmap|satelite]
	size=[BREITExH�HE]
	zoom=[1..21]
	language=[de,en,es,ru,...]
	region=[de,en,es,ru,...]
	center=[STADTNAME]
	
Beispiel: [DHVS2M maptype="roadmap" size="640x480" zoom="5" language="de" region="eu" center="K�ln"]

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets 
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png` 
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
* Initial Version

