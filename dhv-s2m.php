<?php
/*
 Plugin Name: DHV Streckmittelmeldungen
Plugin URI: http://hanfverband.de
Description: Streckmittelmeldungen des <a href="http://hanfverband.de">Deutschen Hanf Verbandes</a>
Version: 1.0.0
Author: WeedCube Software
Author URI: http://weed-cube.com
License: GPL2
*/

/* initialize globals */
global $dhv_s2m_option;
$dhv_s2m_option = 'dhv-s2m-options';

require_once('lib/dhv-s2m-user.php');
if (is_admin()){
	require_once('lib/dhv-s2m-admin.php');
}

/* hook plugin activation, deactivation and uninstall */
if (is_admin()){
	register_activation_hook(__FILE__, array('DHV_S2M_Plugin', 'activate'));
	register_deactivation_hook(__FILE__, array('DHV_S2M_Plugin', 'deactivate'));
	register_uninstall_hook(__FILE__, array('DHV_S2M_Plugin', 'uninstall'));
}

/* handle plugin activation, deactivation and uninstall */
class DHV_S2M_Plugin
{
	public static function activate()
	{
		global $dhv_s2m_option;
		$opts = array(
			'url_feeds' => 'http://hanfverband.weed-cube.com/?feed=dhv-s2m',
			'url_api' => 'http://hanfverband.weed-cube.com/?feed=dhv-s2m-api',
			'url_report' => 'http://hanfverband.weed-cube.com/streckmittelmeldung'
		);
		add_option($dhv_s2m_option, $opts);
	}

	public static function deactivate()
	{
		global $dhv_s2m_option;
		delete_option($dhv_s2m_option);
	}

	public static function uninstall()
	{
		global $dhv_s2m_option;
		delete_option($dhv_s2m_option);
	}
};

?>
